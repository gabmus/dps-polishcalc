package org.gabmus.polishcalcservice.server;

import org.gabmus.polishcalcservice.ErrorHandler;
import org.gabmus.polishcalcservice.Metadata;
import org.gabmus.polishcalcservice.ServerWorker;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class ServerMain {

    public static void main(String[] argv) {

        int port = Metadata.PORT;

        ServerSocket listeningSocket = null;
        try {
            listeningSocket = new ServerSocket(port);
        } catch (IOException e) {
            ErrorHandler.disgraceExit(e, "Error occurred in server socket creation. Terminating");
        }

        while (true) {
            try {
                Socket connectionSocket = listeningSocket.accept();
                System.out.println("Connecting to client `"+
                        connectionSocket.getInetAddress()+
                        "` on port `"+connectionSocket.getPort()+"`"
                );
                Runnable r = new ServerWorker(connectionSocket);
                Thread t = new Thread(r);
                t.start();
            }
            catch (IOException e) {
                ErrorHandler.disgraceExit(e, "Error occurred in handling connection. Terminating");
            }
        }

    }
}
