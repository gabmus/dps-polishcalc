package org.gabmus.polishcalcservice.client;

import org.gabmus.polishcalcservice.Metadata;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

public class ClientMain {
    public static void main(String[] argv) {

        int port = Metadata.PORT;
        String address = "127.0.0.1";

        Scanner inScanner = new Scanner(System.in);

        while (true) {
            try {
                System.out.println("Provide two integers and an operator (supported operators: `+`, `-`, `*`, `/`, `%`)");
                String arg1 = inScanner.next();
                String arg2 = inScanner.next();
                String operator = inScanner.next();


                Socket clientSocket = new Socket(address, port);

                PrintStream toServer = new PrintStream(clientSocket.getOutputStream());
                Scanner fromServerScanner = new Scanner(clientSocket.getInputStream());

                toServer.println(arg1 + "\n" + arg2 + "\n" + operator);
                String result = fromServerScanner.nextLine();
                if (result.toLowerCase().contains("error")) {
                    System.err.println(result);
                    fromServerScanner.close();
                    clientSocket.close();
                } else {
                    System.out.println(result);
                    fromServerScanner.close();
                    clientSocket.close();

                    break;
                }
            }
            catch (IOException e) {
                System.err.println("Connection error, retrying");
            }
        }
        inScanner.close();
    }
}