package org.gabmus.polishcalcservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class ServerWorker implements Runnable {

    public static String calculate(String message) {
        String error;
        String[] msgSplit = message.split("\n");
        int op1 = 0;
        int op2 = 0;
        try {
            op1 = Integer.parseInt(msgSplit[0]);
            op2 = Integer.parseInt(msgSplit[1]);
        }
        catch (NumberFormatException e) {
            error = "Error: Invalid input received";
            System.err.println(error);
            return error;
        }
        String opSign = msgSplit[2];
        switch (opSign) {
            case "+":
                return Integer.toString(op1+op2);
            case "-":
                return Integer.toString(op1-op2);
            case "*":
                return Integer.toString(op1*op2);
            case "/":
                return Integer.toString(op1/op2);
            case "%":
                return Integer.toString(op1%op2);
            default:
                error = "Error: Invalid operation received";
                System.err.println(error);
                return error;
        }
    }

    public Socket socket;

    public ServerWorker(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            BufferedReader inFromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintStream outToClient = new PrintStream(socket.getOutputStream());
            String data = "";
            for (int i = 0; i < 3; i++) {
                data += inFromClient.readLine()+"\n";
            }
            outToClient.println(ServerWorker.calculate(data));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            socket.close();

        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
