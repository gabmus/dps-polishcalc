out/%.class: src/%.java
	javac -d out -cp out src/$*.java

SOURCES = src/org/gabmus/polishcalcservice/Metadata.java \
		src/org/gabmus/polishcalcservice/ErrorHandler.java \
		src/org/gabmus/polishcalcservice/ServerWorker.java \
		src/org/gabmus/polishcalcservice/client/ClientMain.java \
		src/org/gabmus/polishcalcservice/server/ServerMain.java \

all: $(patsubst src/%,out/%,$(patsubst %.java,%.class,$(SOURCES)))

clean:
	rm -rf out/*
